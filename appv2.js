var express = require("express");
var requestJSON = require("request-json");
var bodyParser = require('body-parser');

var app = express();
var port = process.env.port || 3001;
const URLbase = "/api-uruguay/v2";
var baseMLabURL = "https://api.mlab.com/api/1/databases/techu-uruguay/collections/";
var apikeyMLab = "apiKey=w-eSH61NrGOOTM3Ci5kuZnYbkAdFedZ7";


app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// GET users consumiendo API REST de mLab
app.get(URLbase + "/users", 
function(req, res) {
    
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET con id en mLab
app.get(URLbase + "/users/:id",
  function (req, res) {
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});


// POST 'users' mLab
app.post(URLbase + "/users",
  function(req, res){
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?s={"id": 1}&' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = (body[body.length-1].id) + 1;
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "gender" : req.body.gender,
          "password" : req.body.password
        };
        clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
          function(error, respuestaMLab, body){
            res.send(body);
          });
      });
  });

  // PUT 'users' mLab
app.put(URLbase + "/users/:id",
    function(req, res){
        var clienteMlab = requestJSON.createClient(baseMLabURL);
        var id = req.params.id;
        var queryString = 'q={"id":' + id + '}&';

        var newUser = {
            "id" : parseInt(id),
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "gender" : req.body.gender,
            "password" : req.body.password
        };
        
        clienteMlab.put(baseMLabURL + "user?" + queryString + apikeyMLab, newUser,
            function(error, respuestaMLab, body){
                res.send(body);
            }
        );
    
    }
);



// DELETE 'users' mLab
app.delete(URLbase + "/users/:id",
function(req, res){
  var clienteMlab = requestJSON.createClient(baseMLabURL);
  var id = req.params.id;
  var queryString = 'q={"id":' + id + '}&';
  
  clienteMlab.get('user?' + queryString + apikeyMLab,
    function(error, respuestaMLab, body){
        if (body[0] != undefined){
          clienteMlab.del(baseMLabURL + "user/" + body[0]._id.$oid +"?" + apikeyMLab, 
              function(error, respuestaMLab, body){
                  res.send(body);
              });
          } else {
              res.send(response = {
                  "msg" : "Usuario no encontrado"
                });
          }
      });
        
  });

  app.post(URLbase + "/login",
    function (req, res){
        console.log(req.body);
        
        var email = req.body.email;
        var pass = req.body.password;
        var queryStringEmail = 'q={"email":"' + email + '"}&';
        console.log(queryStringEmail);
        
        var  clienteMlab = requestJSON.createClient(baseMLabURL);
        clienteMlab.get('user?'+ queryStringEmail + apikeyMLab ,
        function(error, respuestaMLab , body) {
        var respuesta = body[0];
        console.log(body);
        
        if(respuesta != undefined){
            if (respuesta.password == pass) {
                var session = {"logged":true};
                var login = '{"$set":' + JSON.stringify(session) + '}';
                clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
                function(errorP, respuestaMLabP, bodyP) {
                    res.send(body[0]);
                });
            }
            else {
                res.send({"msg":"datos ingresados no válidos"});
            }
        } else {
            res.send({"msg": "datos ingresados no válidos"});
        }
        });
    });


//Method POST logout
app.post(URLbase + "/logout",
  function (req, res){
    var email = req.body.email;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab, body) {
      var respuesta = body[0];
      if(respuesta != undefined){
            var session = {"logged": true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);
              });
      } else {
        res.send({"msg": "Error en logout"});
      }
    });
});


app.listen(port);
console.log("Escuchando ...");
