#imagen docker base, de la cual se parte
FROM node:latest

#directorio de trabajo de docker
WORKDIR /docker-api

#copiar archivos del proyecto al workdir
ADD . /docker-api

#instala las dependencias del proyecto
#RUN npm install

#puerto donde exponemos el contenedor
EXPOSE 3001

#comando para lanzar la app
CMD ["npm", "run", "prod"]