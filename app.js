var express = require("express");
var usersFile = require("./users.json");
var accountsFile = require("./accounts.json");
var bodyParser = require('body-parser');

var app = express();
var port = process.env.port || 3000;
const URI = "/api-uruguay/v1";

app.use(bodyParser.json());


app.get(URI + "/users", 
    function (req, res) {
        //res.sendFile("users.json", {root: __dirname});
        res.send(usersFile);
    }
);

app.get(URI + "/users/:id", 
    function (req, res) {
        let id = req.params.id;
        res.send(usersFile[id -1]);
    }
);

app.get(URI + "/accounts", 
    function (req, res) {
        res.send(accountsFile);
    }
);

app.get(URI + "/accounts/:id", 
    function (req, res) {
        let id = req.params.id;
        res.send(accountsFile[id -1]);
    }
);


app.post(URI + "/users", 
    function (req, res) {
        let size = usersFile.length - 1;
        let user = { 
            'id': size,
            'first_name': req.headers.first_name,
            'last_name': req.headers.last_name,
            'email': req.headers.email,
            'gender': req.headers.gender,
            'password': req.headers.password
        };
        usersFile.push(user);
        res.send({"msg" : "Usuario creado correctamente", user});
        
        
    }
);

app.post(URI + "/users2", 
    function (req, res) {
        let size = usersFile.length - 1;
        let user = { 
            'id': size,
            'first_name': req.body.first_name,
            'last_name': req.body.last_name,
            'email': req.body.email,
            'gender': req.body.gender,
            'password': req.body.password
        };
        usersFile.push(user);
        res.send({"msg" : "Usuario creado correctamente", user});
        
    }
);

app.put(URI + "/users/:id", 
    function (req, res) {
        let id = req.params.id;
        let existe = true;
        
        if (usersFile[id -1] == undefined) {
            existe = false;
        } else {
            let user = { 
                'id': id,
                'first_name': req.body.first_name,
                'last_name': req.body.last_name,
                'email': req.body.email,
                'gender': req.body.gender,
                'password': req.body.password
            };
            usersFile[id -1] = user;
        }
        let msg = existe ? {"msg" : "Usuario modificado correctamente"} : {"msg" : "No fue posible modificar el usuario"};
        let statusCode = existe ? 200 : 400;

        res.status(statusCode).send(msg);
    }
);

app.delete(URI + "/users/:id", 
    function (req, res) {
        let id = req.params.id;
        let existe = true;
        
        if (usersFile[id -1] == undefined) {
            existe = false;
        } else {
            usersFile.splice(usersFile[id -1], 1);
        }
        let msg = existe ? {"msg" : "Usuario eliminado correctamente"} : {"msg" : "No fue posible eliminar el usuario"};
        let statusCode = existe ? 200 : 400;

        res.status(statusCode).send(msg);
    }
);

app.get(URI + "/user", 
    function (req, res) {
        console.log("query users: " + req.query.first_name + " - " + req.query.last_name);
        let encontrado = false;
        
        for (let i = 0; (i<usersFile.length) && !encontrado; i++){
            if ((usersFile[i].first_name == req.query.first_name) && (usersFile[i].last_name == req.query.last_name)) {
                let user = usersFile[i];
                res.status(200).send({"msg" : "Usuario encontrado", user});
                encontrado = true;
            }
        }
        if (!encontrado) {
            res.status(404).send({"msg" : "Usuario no encontrado"});
        }
    }
);


app.listen(port);
console.log("Escuchando ...");
